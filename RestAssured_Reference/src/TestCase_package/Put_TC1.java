package TestCase_package;

import java.io.File;
import java.io.IOException;
import java.time.LocalDateTime;
import org.testng.Assert;
import org.testng.annotations.Test;
import API_common_methods.Common_Put_Method_;
import Utility_common_methods.Handle_api_logs;
import Utility_common_methods.Handle_directory;
import endpoint.Put_endpoint;
import io.restassured.path.json.JsonPath;
import request_repository.Put_request_repository;

public class Put_TC1 extends Common_Put_Method_ {
	@Test
	public static void executor() throws IOException {
		File log_dir = Handle_directory.create_log_directory("Put_TC1_logs");
		String requestBody = Put_request_repository.Put_request_tc1();
		String endpoint = Put_endpoint.Put_endpoint_tc1();
		for (int i = 0; i < 5; i++) {
			int statusCode = put_statusCode(requestBody, endpoint);
			System.out.println(statusCode);
			if (statusCode == 200) {

				String responseBody = put_responseBody(requestBody, endpoint);
				System.out.println(responseBody);
				Handle_api_logs.evidence_creator(log_dir, "Put_TC1", endpoint, requestBody, responseBody);
				Put_TC1.validator(requestBody, responseBody);
				break;

			} else {
				System.out.println("Expected statuscode is not found hence retrying");
			}
		}
	}

	public static void validator(String requestBody, String responseBody) {

		JsonPath jsp_req = new JsonPath(requestBody);
		String req_name = jsp_req.getString("name");
		String req_job = jsp_req.getString("job");

		LocalDateTime Currentdate = LocalDateTime.now();
		String updatedAt = Currentdate.toString().substring(0, 11);
		JsonPath jsp_res = new JsonPath(responseBody);

		String res_name = jsp_res.getString("name");
		String res_job = jsp_res.getString("job");
		String res_date = jsp_res.getString("updatedAt");
		res_date = res_date.substring(0, 11);

		Assert.assertEquals(res_name, req_name);
		Assert.assertEquals(res_job, req_job);
		Assert.assertEquals(res_date, updatedAt);

	}
}
