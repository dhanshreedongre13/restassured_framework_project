**** RestAssured Framework Project ****

We have implemented our *** RestAssured Framework Project *** incorporating 

⮚	Data Driven
⮚	Keyword Driven

My framework is combination of keyword driven framework and where in I am having test Script,testcases which are also being executed on the basic of data.
My framework can be divided into seven major components:-

⮚ Test Driver Mechanism.
⮚ Test Script. 
⮚ Data Variable File.
⮚ API Related Common Fuction. 
⮚ Utility Common Methods.
⮚ Libraries.
⮚ Report. 

**Test Driver Mechanism** : In these packages, we have the two driver mechanism one is staic and dynamic driver class in our Test Driver packages.In which we are calling the test scripts in static class and calling them in our main method for execution and apart from that we have created a new dynamic class as well in which we are able to fetch the test cases to be executed from the user from an excel file and dynamically call the test scripts on run time and this is being done by "java.lang.reflect".

**Test Script** : In this we have all the available test scripts those needs to be created.

**Data/Variable File** : We are using the excel files to input the data to our framework or requestBody parameters to input from excel files.

**API related common functions** : Which are used to trigget the API, extract the responseBody & extract the response status code.

**Utilities Common Methods** : We have class to create the directory, if it doesn't exist in the project for test log files and if it does exist than to delete the old one and create the new one for log files. Also we have another utility from which all the endpoint, requestBody, responseBody can be stored in the notepad/text file. And another utility to read the value/data from an excel file.

**Libraries** : We are using RestAssured, Apache-Poi, testng libraries and dependency management of libraries are being managed by maven repository.

**Reports** : We are using Allure reports and Extent reports.


